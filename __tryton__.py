# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Tax Cash Basis Taxes for Germany',
    'name_de_DE': 'Buchhaltung Gültigkeitsdauer Steuer ' \
            'Ist-Versteuerung für Deutschland',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Tax Data for cash basis accounting in Germany
    - Provides tax data for german taxes (cash-based)
''',
    'description_de_DE': '''Steuerdaten für Deutschland (Ist-Versteuerung)
    - Fügt Steuerdaten für Ist-Versteuerung in Deutschland hinzu.
''',
    'depends': [
        'account_tax_cash_basis',
        'account_timeline_tax_de',
    ],
    'xml': [
        'account.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
